Esempi di utilizzo:
    ./fit.py inv/*.csv
    ./fit.py non-inv/*.csv
    ./fit.py -o ouptut.txt non-inv/*.csv # Come sopra, ma salva i risultati nel file output.txt

    ./fit.py --db --rad -t passa-basso passa-basso/*.csv
    ./fit.py --db --rad -t passa-alto passa-alto/*.csv
    ./fit.py --db --rad -t passa-alto -f 5 -P 0.02 passa-alto/*.csv # Stampa un grafico solo se la distanza
                                                                    # quadratica normalizzata supera 0.02

    ./fit.py --db --rad -c -t passa-alto passa-alto/*.csv # Stampa anche in forma complessa
    ./fit.py --db --rad -f 7 -t passa-alto passa-alto/*.csv # Prova con 7 frequenze di guess anzichè 3
                                                            # (inutile in questo caso)


Opzioni:
    --version             Mostra la versione del programma ed esce
    -h, --help            Mostra quuesto messaggio di aiuto ed esce
    -d, oppure equivalentemente  --db:   output in dB anzicchè lineare
    -r, oppure equivalentemente  --rad:  output in radianti anzichè in gradi
    -t [tipo], --type=[tipo] Il tipo di filtro (opzionale, default none)
        Attualmente sono supportati solo i due seguenti tipo di filtro:
        - passa-basso: un passa-basso a singolo polo
        - passa-alto:  un passa-alto a singolo polo
        - none: non esegue nessun fit del filtro
    -v, --verbose: abilita l'output verboso
    -c, --complex: stampa Vout/Vin come numero complesso
    -s YSIGMA, --sigma=YSIGMA errore sulla tensione
                              [default prova ad indovinare dai dati]
    -p, --plot:    plotta i fit di ciascun file csv
    -m MAXEVAL, --max-evals=MAXEVAL  numero massimo di iterazioni
                                     del least-square. Default 1000
    -f NFREQ, --freq-guess=NFREQ     Numero di starting guess nelle frequenze
                                     per l'algoritmo di fit. Provare ad
                                     incrementarlo se qualche fit non viene.
                                     default: 3

