#! /usr/bin/python
import sys
import os
import numpy as np
from scipy.optimize import curve_fit
from scipy.optimize import leastsq
from numpy.fft import fft
import matplotlib.pyplot as plt

os.environ['QT_LOGGING_RULES'] = "qt5ct.debug=false"

def printsignificant(num, inc):
    if inc > 1:
        print(num, "+-", inc, end='')
        return
    if num < 0:
        print("-", end='')
        num = -num
    digits = 1 - int(np.floor(np.log10(inc)))
    integerpart = int(np.floor(num))
    decimalpart = num - integerpart
    decimalpart = int(np.round(decimalpart * (10 ** digits)))
    # Decimal pad
    zpad = digits - 1 - (int(np.floor(np.log10(decimalpart))) if decimalpart != 0 else 0)
    if zpad < 0:
        integerpart += 1
        decimalpart -= int(10 ** digits)
        zpad = digits - 1
    print(integerpart, ".", end='', sep='')
    for i in range(0, zpad):
        print("0", end='')
    print(decimalpart, end='', sep='')
    print("(", end='')
    print(int(inc*(10**digits)), end='')
    print(")", end='')

def printsignificant_complex(num, inc):
    printsignificant(np.real(num), np.real(inc))
    if np.imag(num) > 0:
        print(" + j*", end='')
    else:
        print(" - j*", end='')
    printsignificant(np.abs(np.imag(num)), np.imag(inc))

def zero_twopi(x):
     return x - np.floor(x/ (2*np.pi))*2*np.pi

def armonize_angles(x):
    for i in range(1, len(x)):
        x[i] = x[i] - np.around((x[i] - x[i-1])/(2*np.pi))*(2*np.pi)
    return x
        

def coseno(t, f, A0, phi0, offset):
    return A0*np.cos(2*np.pi*f*t + phi0) + offset

from optparse import OptionParser
parser = OptionParser(usage="usage: %prog [options] file1, [file2, [...]]", version="%prog 0.2")
parser.add_option("-r", "--rad", dest="radians", action="store_true",
                  help="Uses radians instead of degrees", default=False)
parser.add_option("-d", "--db", dest="decibel", action="store_true",
                  help="Uses decibiel for amplitude ratios", default=False)
parser.add_option("-p", "--plot", dest="plot", action="store_true",
                  help="Plots the data on the screen", default=False)
parser.add_option("-P", "--plot-threshold", dest="plot_threshold", action="store", type="float",
                  help="Plots the data whose L2 norm interpolation exceeds the threeshold", default=float('nan'))
parser.add_option("-v", "--verbose", dest="verbose", action="store_true",
                  help="Verbose output", default=False)
parser.add_option("-c", "--complex", dest="cmplx", action="store_true",
                  help="Prints result also as caresian complex number", default=False)
parser.add_option("-m", "--max-evals", dest="maxeval", action="store", type="int",
                  help="Maximum evaluations for leastsquare", default=1000)
parser.add_option("-f", "--freq-guess", dest="nfreq", action="store", type="int",
                  help="Number of frequency guess for leastsquare", default=3)
parser.add_option("-s", "--sigma", dest="ysigma", action="store", type="float",
                  help="Error on voltage [default tries to guess]", default=0)
parser.add_option("-t", "--type", dest="type", action="store", type="string",
                  help="Filter type: it can be 'passa-basso' or 'passa-alto' or 'none", default="none")
parser.add_option("-o", "--output", dest="outputfile", action="store", type="string",
                  help="Output file, saves in cvs format", default="")
parser.add_option("-u", "--use-cols", dest="usecols", action="store", type="string",
                  help="Columns to use from the input file in the format \"time, vin, vout\"", default="0,1,2")

(options, args) = parser.parse_args()

if len(args) == 0:
    print("No filename provided")
    exit(0)

try:
    tousecols = np.array(options.usecols.split(','), dtype=int)
except ValueError as e:
    print(e)
    print("Please provide integer column numbers")
    exit(0)
    
if len(tousecols) != 3:
    print("Expected 3 columns, provided {}".format(len(tousecols)))
    exit(0)

tousecol_lt_zero = len(tousecols[np.where(tousecols < 0)])
if tousecol_lt_zero != 0: # Tutte le colonne devono essere > 0
    print("provided {} column(s) less than zero".format(tousecol_lt_zero))
    exit(0)
del tousecol_lt_zero

angleconversion = 180/np.pi
if options.radians:
    angleconversion = 1.0

if options.type != "passa-alto" and options.type != "passa-basso" and options.type != "none":
    print("Devi indicare il tipo di filtro")
    print("Ri chiamami con l'opzione -t [passa-alto|passa-basso|none]")
    exit(0)

outfile = None
if options.outputfile != "":
    aoutainm = "Num"
    if options.decibel:
        aoutainm = "dB"
    phasem = "deg"
    if options.radians:
        phasem = "rad"
    outfile = open(options.outputfile, "w+")
    outfile.write(("Frequenza (Hz), err (Hz), Aout (V), err (V), Ain (V), err (V),"+
                   "Aout/Ain ({0}), err ({0}), Sfasamento ({1}), err ({1})").format(aoutainm, phasem))
    if options.cmplx:
        outfile.write(", Real (Num), err (Num), Imag (Num), err (Num)")
    outfile.write('\n')
        

def trybestfit(xdata, ydata, ysigma):
    # Calcola la fft per "indovinare" una buona frequenza di partenza
    rawdatafft = fft(np.multiply(ydata, np.hamming(len(ydata))))
    datafft = np.abs(rawdatafft)
    argmaxfft = np.argmax(datafft[:int(len(datafft)/2)])
    n = 3 # I migliori risultati sembrano esserci per n = 3
    maxbin = argmaxfft + n + 1 if argmaxfft + n < int(len(datafft)/2) - 1 else int(len(datafft)/2) -1
    minbin = argmaxfft - n if argmaxfft > n else 0
    avgbin = np.dot(datafft[minbin:maxbin], range(minbin, maxbin))/np.sum(datafft[minbin:maxbin])
    binfspan = 1.0/np.average(np.diff(xdata))/len(datafft)
    freqguess = avgbin*binfspan
    cof = 1.0/(maxbin-minbin)
    argguess = np.arctan2(cof * np.sum(np.sin(np.angle(rawdatafft[minbin:maxbin]))),
                          cof * np.sum(np.cos(np.angle(rawdatafft[minbin:maxbin]))))
    argguess = zero_twopi(argguess + 2*np.pi)

    fmin = freqguess - binfspan/2
    fmax = freqguess + binfspan/2

    param_bounds = np.array([[fmin, 0, argguess - 2*np.pi, 0],
                             [fmax, 0, argguess + 2*np.pi, 0]])
    
    if options.nfreq > 1:
        freqguessvec = np.linspace(fmin, fmax, options.nfreq)
    else:
        freqguessvec = np.array([freqguess])

    bestsquared = None
    for freqguess in freqguessvec: # Per ogni guessfreq
        maxydataarg = np.argmax(np.abs(ydata))
        maxydata = np.abs(ydata[maxydataarg])
        param_bounds[1][1] = maxydata + ysigma[maxydataarg]
        param_bounds[0][1] = max(maxydata*0.75 - ysigma[maxydataarg], 0)
        initial_guess = np.zeros(4)
        initial_guess[0] = freqguess
        initial_guess[1] = param_bounds[1][1] - np.amax(ysigma)/4;
        initial_guess[2] = argguess
        initial_guess[3] = np.average(ydata)
        param_bounds[0][3] = min(-param_bounds[1][1]*0.25, initial_guess[3]) # 25% dell'altezza massima
        param_bounds[1][3] = max( param_bounds[1][1]*0.25, initial_guess[3])
        
        if options.verbose:
            print(" + Fit options [min, guess, max]")
            print(" + Freq: [", param_bounds[0][0],
                                initial_guess[0],
                                param_bounds[1][0], "]")
            print(" + Amp   [",  param_bounds[0][1],
                                 initial_guess[1], 
                                 param_bounds[1][1], "]")
            print(" + Phase [",  param_bounds[0][2],
                                 initial_guess[2], 
                                 param_bounds[1][2], "]")
            print(" + Zero  [",  param_bounds[0][3],
                                 initial_guess[3], 
                                 param_bounds[1][3], "]")

        tpopt, tpcov = curve_fit(coseno, xdata, ydata, sigma=ysigma, absolute_sigma=True, p0=initial_guess, bounds=param_bounds, maxfev=options.maxeval, gtol=1E-10)
        squared = np.linalg.norm(ydata - coseno(xdata, *tpopt)) # Test della bontà del risultato

        if bestsquared == None or squared < bestsquared:
            bestsquared = squared
            popt, pcov = tpopt, tpcov
        
        if options.verbose:
            print(" + Square distance ", end='')
            print(squared)

    return popt, pcov, bestsquared

def guessysigma(data):
    return np.unique(np.abs(np.diff(data)))[1]

bodeanalysis = []
for filename in args:
    tdata, ydatach1, ydatach2 = np.loadtxt(filename, delimiter=',', usecols=tousecols, unpack=True)
    tdata -= np.average(tdata) # Esegue uno shift temporale, in modo che i dati siano 'centrati'
    ch1avg = np.average(ydatach1)
    ch2avg = np.average(ydatach2)
    ydatach1 -= ch1avg # Centra i dati su entrambi i canali
    ydatach2 -= ch2avg

    print("File:", filename)
    if options.ysigma != 0:
        ysigmach1 = options.ysigma*np.ones(ydatach1.size)
        ysigmach2 = options.ysigma*np.ones(ydatach2.size)
    else:
        ch1sigmaguess = guessysigma(ydatach1)
        ch2sigmaguess = guessysigma(ydatach2)
        if options.verbose:
            print(" + No sigma provided, guessing: ")
            print("   - ch1: ", ch1sigmaguess, "Volts")
            print("   - ch2: ", ch2sigmaguess, "Volts")
        ysigmach1 = ch1sigmaguess*np.ones(ydatach1.size)
        ysigmach2 = ch2sigmaguess*np.ones(ydatach2.size)
        del ch1sigmaguess
        del ch2sigmaguess
    
    poptch1, pcovch1, squaredch1 = trybestfit(tdata, ydatach1, ysigmach1) # Analisi dell'ingresso
    poptch2, pcovch2, squaredch2 = trybestfit(tdata, ydatach2, ysigmach2) # Analisi dell'uscita

    if options.verbose:
        print(" + Opzioni ch1 [freq, Amp, phi, y0, offset]: ", end='')
        print(poptch1 + [0, 0, 0, ch1avg])
        print(" + Covarianze ch1 [freq, Amp, phi, offset]x[freq, Amp, phi, offset]: ")
        print(pcovch1)
        print(" + Opzioni ch2 [freq, Amp, phi, offset]: ", end='')
        print(poptch2 + [0, 0, 0, ch2avg])
        print(" + Covarianze ch2 [freq, Amp, phi, offset]x[freq, Amp, phi, offset]: ")
        print(pcovch2)

    # Usa la media dei due canali
    # freq = (poptch2[0] + poptch1[0])/2
    # freqerror = (np.sqrt(pcovch1[0][0])+np.sqrt(pcovch2[0][0]))/2
    # Usa solo il canale di ingresso
    freq = poptch1[0]
    freqerror = np.sqrt(pcovch1[0][0])
    print("   Frequenza:  ", end='')
    if freq < 1E3:
        printsignificant(freq, freqerror)
        print(" ", end='')
    elif freq < 1E6: # Stampa in kHz
        printsignificant(freq/1E3, freqerror/1E3)
        print(" k", end='')
    else: # Stampa in MHz
        printsignificant(freq/1E6, freqerror/1E6)
        print(" M", end='')
    print("Hz")
    if options.verbose:
        print("   Aout:       ", end='')
        printsignificant(poptch2[1], np.sqrt(pcovch2[1][1]))
        print(" Volt")
        print("   Ain:        ", end='')
        printsignificant(poptch1[1], np.sqrt(pcovch2[1][1]))
        print(" Volt")
    print("   Aout/Ain:   ", end='')
    aratio = poptch2[1]/poptch1[1]
    aratioerror = aratio*(np.sqrt(pcovch1[1][1])/poptch1[1] + np.sqrt(pcovch2[1][1])/poptch2[1])
    if options.decibel:
        aratiodb = 20 * np.log10(aratio) # Formula per la conversione in db in potenza
        aratioerrorprnt = 20*aratioerror/aratio # Propagazione dell'errore
        aratioprnt = aratiodb
    else:
        aratioprnt = aratio
        aratioerrorprnt = aratioerror
    printsignificant(aratioprnt, aratioerrorprnt)

    if options.decibel:
        print(" dB")
    else:
        print()
    print("   Sfasamento: ", end='')
    phasedelta = zero_twopi(poptch2[2] - poptch1[2]) # In radianti
    phasedeltaerror = (np.sqrt(pcovch1[2][2])+np.sqrt(pcovch2[2][2]))
    # phasedeltaprnt = phasedelta if phasedelta < np.pi else phasedelta - 2*np.pi
    if len(bodeanalysis) != 0:
        phasedelta -= np.around((phasedelta - zero_twopi(np.angle(bodeanalysis[-1]['nyquist'])))/(2*np.pi))*(2*np.pi)
    printsignificant(phasedelta*angleconversion,
                     phasedeltaerror*angleconversion)
    if options.radians:
        print(" rad")
    else:
        print(" deg")
    
    if options.verbose:
        print("   Offset: ")
        print("    + Canale 1: ", end='')
        printsignificant(poptch1[3], np.sqrt(pcovch1[3][3]))
        print(" Volt")
        print("    + Canale 2: ", end='')
        printsignificant(poptch2[3], np.sqrt(pcovch2[3][3]))
        print(" Volt")

    real = aratio*np.cos(phasedelta)
    realerror = np.abs(aratio*np.sin(phasedelta)*phasedeltaerror) + np.abs(aratioerror*np.cos(phasedelta))
    imag = aratio*np.sin(phasedelta)
    imagerror = np.abs(aratio*np.cos(phasedelta)*phasedeltaerror) + np.abs(aratioerror*np.sin(phasedelta))
    bodeanalysis.append({"freq":freq, "freqerror":freqerror, "nyquist":real + 1j*imag, "nyquisterror":realerror+1j*imagerror})

    if options.cmplx: # Prints in complex form
        print("   Complex output: ", end='')
        printsignificant_complex(bodeanalysis[-1]['nyquist'], bodeanalysis[-1]['nyquisterror'])
        print()
    
    if outfile != None:
        outfile.write("{}, {}, {}, {}, {}, {}, {}, {}, {}, {}".format(
                      freq, freqerror, poptch2[1], np.sqrt(pcovch2[1][1]), poptch1[1], np.sqrt(pcovch2[1][1]),
                      aratioprnt, aratioerrorprnt, phasedelta*angleconversion, phasedeltaerror*angleconversion))
        if options.cmplx:
            outfile.write(", {}, {}, {}, {}".format(real, realerror, imag, imagerror))
        outfile.write('\n')
    
    squared_threes = np.maximum(squaredch1/(np.average(ysigmach1)*len(ysigmach1)), squaredch2/(np.average(ysigmach2)*len(ysigmach2)))
    if options.verbose:
        print("Square distance: ", squared_threes)
    if options.plot or options.plot_threshold <= squared_threes:
        taxis = tdata - tdata[0] # In modo tale che il primo campione sia lo zero dei tempi
        plt.plot(taxis, ydatach1, 'b-')
        plt.plot(taxis, coseno(tdata, *poptch1), 'g--')
        plt.plot(taxis, ydatach2, 'r-')
        plt.plot(taxis, coseno(tdata, *poptch2), 'k--')
        plt.show();

# end for
if outfile != None:
    outfile.close() # Fine analisi. Salva il file se esiste

bodeanalysis.sort(key=lambda x: x['freq']) # Ordina per frequenza
if options.verbose:
    for field in bodeanalysis:
        print(field)
nyquists = np.array([ bodeanalysis[i]['nyquist'] for i in range(len(bodeanalysis)) ])
frequencies = np.array([ bodeanalysis[i]['freq'] for i in range(len(bodeanalysis)) ])
nyquisterrors = np.array([ bodeanalysis[i]['nyquisterror'] for i in range(len(bodeanalysis)) ])
frequencieserror = np.array([ bodeanalysis[i]['freqerror'] for i in range(len(bodeanalysis)) ])

# Fit del diagramma di Bode
def bode_passa_basso(f, fpolo): # tpolo = 1/(2pi*fpolo)
    return 1/(1+1j*f/fpolo) # PASSA BASSO

def bode_passa_alto(f, fpolo): # tpolo = 1/(2pi*fpolo)
    return 1j*f/(fpolo+1j*f) # PASSA ALTO

def leastsq_helper(params, bodef, fdata, nyqdata, nyquisterrors):
    diff = nyqdata - bodef(fdata, *params)
    return np.sqrt(np.divide(diff.real, nyquisterrors.real) ** 2 +
                   np.divide(diff.imag, nyquisterrors.real) ** 2)

if options.type == "passa-alto":
    bode = bode_passa_alto
elif options.type == "passa-basso":
    bode = bode_passa_basso

if options.type != "none":
    x0 = [5000] # Guess iniziale
    nyquisterrorstot = nyquisterrors
    res = leastsq(leastsq_helper, x0, full_output=1,
                   args=(bode, frequencies, nyquists, nyquisterrorstot))
    fpolo = res[0][0]
    fpoloinc = np.sqrt(res[1][0][0])
    # NOTA: Gli errori sulla frequenza sono assolutamente trascurabili
    samplefreqs = np.logspace(np.log10(frequencies[0]*0.7), np.log10(frequencies[-1]/0.7), 250)
    expectedvals = bode(samplefreqs, fpolo)
    print("Il polo del circuito è: ", end='')
    printsignificant(fpolo/1000, fpoloinc/1000)
    print("kHz")
    # print(popt.x) # Polo
    # print(bode_passa_basso(frequencies, *popt.x)) # Valori best fit

ptstyle = {'linestyle':'', 'marker':'o', 'color':'k', 'markersize':2} # Stile del grafico
if options.type == "none": # Senza interpolazioni linea continua
    ptstyle = {'linestyle':'-', 'marker':'o', 'color':'k', 'linewidth':.5, 'markersize':2}

# Window no 1 Bode
plt.figure(1).canvas.set_window_title('Bode')
plt.subplot(211)
if options.type != "none":
    plt.plot(samplefreqs, np.abs(expectedvals), 'r-', linewidth=1)
plt.plot(frequencies, np.abs(nyquists), **ptstyle)
plt.grid(which='both', linestyle='--')
plt.yscale('log')
plt.xscale('log')
plt.subplot(212)
plt.xscale('log')
plt.grid(which='both', linestyle='--')
if options.type != "none":
    plt.plot(samplefreqs, angleconversion*armonize_angles(np.angle(expectedvals)), 'r-', linewidth=1)
plt.plot(frequencies, angleconversion*armonize_angles(np.angle(nyquists)), **ptstyle)
plt.draw()

# Window no 2 Nyquist
plt.figure(2).canvas.set_window_title('Nyquist')
if options.type != "none":
    plt.plot(np.real(expectedvals), np.imag(expectedvals), 'r-', linewidth=1)
plt.plot(np.real(nyquists), np.imag(nyquists), **ptstyle)
plt.draw()

# Mostra tutti i grafici
plt.show()

